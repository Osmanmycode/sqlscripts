use modernways;
set sql_safe_updates = 0;
UPDATE modernways.huisdieren 
SET 
    huisdieren.Leeftijd = 9
WHERE
    huisdieren.Baasje = 'Christiane'
        OR huisdieren.Baasje = 'Bert';
    set sql_safe_updates = 1;