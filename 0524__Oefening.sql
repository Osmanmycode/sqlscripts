use modernways;
set sql_safe_updates = 0;
DELETE FROM huisdieren 
WHERE
    (huisdieren.soort = 'hond'
    AND huisdieren.Baasje = 'Thaïs')
    OR (huisdieren.soort = 'kat'
    AND huisdieren.Baasje = 'Truus');
set sql_safe_updates = 1;